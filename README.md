# delta-en-take5-sl

----------------------------
**Version number:**  3.0.2 
**Framework versions:**  5.2+  
**Author / maintainer:** Adapt Core Team / DeltaNet  
**Accessibility support:** WAI AA  
**RTL support:** Yes  
**Cross-platform coverage:** Chrome, Chrome for Android, Firefox (ESR + latest version), Edge, IE11, Safari 12+13 for macOS/iOS/iPadOS, Opera  
